from django.db import models
from django.utils.translation import ugettext_lazy as _


class Technologies(models.Model):
    """
    Модель, описывающая технологию, которая используется в проекте
    """

    title = models.CharField(max_length=50, verbose_name=_("Название технологии"))
    icon = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("Название иконки технологии"))

    class Meta:
        verbose_name = _("Технология")
        verbose_name_plural = _("Технологии")

    def __str__(self):
        return self.title
