from django.contrib import admin

from src.apps.core.models import SocialNetworks, Project, About

admin.site.register(SocialNetworks)
admin.site.register(Project)
admin.site.register(About)
