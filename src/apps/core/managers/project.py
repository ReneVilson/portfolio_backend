from django.db import models

from django.apps import apps


class ProjectManager(models.Manager):
    """
    Менеджер для модели Project. Возвращает только опубликованные проекты
    """

    def get_queryset(self):
        return super().get_queryset().filter(public=True)
