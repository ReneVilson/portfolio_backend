from django.db import models
from django.utils.translation import ugettext_lazy as _

from src.apps.common.models import Technologies
from src.apps.core.managers import ProjectManager


class Project(models.Model):
    """Класс, с описанием личных проектов
    """

    title = models.CharField(max_length=50, verbose_name=_("Название проекта"))
    description = models.TextField(blank=True, null=True, verbose_name=_("Описание проекта"))
    link = models.URLField(blank=True, null=True, verbose_name=_("Ссылка на проект"))
    image = models.ImageField(blank=True, null=True, verbose_name=_("Изображение проекта"))
    technologies = models.ManyToManyField(
        Technologies,
        related_name="technology",
        verbose_name=_("Используемые технологии")
    )
    public = models.BooleanField(default=False, verbose_name=_("Опубликовано"))

    objects = models.Manager()
    available = ProjectManager()

    class Meta:
        verbose_name = _("Проект")
        verbose_name_plural = _("Проекты")

    def __str__(self):
        return self.title
