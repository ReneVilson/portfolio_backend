from django.db import models
from django.utils.translation import ugettext_lazy as _


class SocialNetworks(models.Model):
    """
    Социальные сети
    """

    title = models.CharField(max_length=50, blank=True, null=True, verbose_name=_("Название социальнй сети"))
    link = models.URLField(max_length=150, blank=True, null=True, verbose_name=_("Адрес социальной сети"))
    username = models.CharField(max_length=50, blank=True, null=True, verbose_name=_("Имя пользователя"))

    class Meta:
        verbose_name = _("Социальная сеть")
        verbose_name_plural = _("Социальные сети")

    def __str__(self):
        return f"{self.title}: {self.username}"
