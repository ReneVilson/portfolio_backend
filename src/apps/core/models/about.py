from django.db import models
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField


class About(models.Model):
    """
    Класс с информацией для главной стринцы
    """

    article = HTMLField(blank=True, null=True, verbose_name=_("Информация"))

    class Meta:
        verbose_name = _("Обо мне")
        verbose_name_plural = _("Обо мне")
