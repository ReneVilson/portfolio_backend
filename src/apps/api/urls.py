from .v1 import urls as api_v1
from django.urls import path, include

urlpatterns = [
    path('v1/', include((api_v1, 'v1'), namespace='v1')),
]


