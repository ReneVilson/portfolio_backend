from rest_framework.generics import ListAPIView

from src.apps.api.v1.serializers import ProjectSerializer
from src.apps.common.paginations import StandardResultsSetPagination
from src.apps.core.models import Project


class GetAllProjectsView(ListAPIView):
    """
    Получение списка всех проектов
    """
    queryset = Project.available.all()
    serializer_class = ProjectSerializer
    pagination_class = StandardResultsSetPagination
