from .all_projects import GetAllProjectsView
from .project import GetProjectView
from .about import AboutView
