from rest_framework.generics import RetrieveAPIView

from src.apps.api.v1.serializers import AboutSerializer
from src.apps.core.models import About


class AboutView(RetrieveAPIView):
    """
    Получение информации для раздела 'Обо мне'
    """
    serializer_class = AboutSerializer

    def get_object(self):
        """
        Предполагается, что запись в бд будет одна.
        """
        return About.objects.first()
