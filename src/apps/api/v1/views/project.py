from rest_framework.generics import RetrieveAPIView

from src.apps.api.v1.serializers import ProjectSerializer
from src.apps.core.models import Project


class GetProjectView(RetrieveAPIView):
    """
    Получение проекта по его id
    """
    lookup_field = "id"
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
