from django.conf.urls import url
from django.urls import path
from drf_yasg import openapi

from src.apps.api.v1.views import GetAllProjectsView, GetProjectView, AboutView
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from src.settings import DOMAIN_URL

urlpatterns = [
    path('projects/', GetAllProjectsView.as_view(), name="projects"),
    path('projects/<int:id>', GetProjectView.as_view(), name="project"),
    path('about/', AboutView.as_view(), name="about"),
]

schema_view = get_schema_view(
    openapi.Info(
        title="API",
        default_version="v1",
        description="API for portfolio",
    ),
    public=True,
    url=DOMAIN_URL,
    permission_classes=(permissions.AllowAny,),
)

swagger_urls = [
    url(r"^docs(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=None), name="schema-json"),
    url(r"^docs/$", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
]

urlpatterns += swagger_urls
