from rest_framework import serializers

from src.apps.core.models import Project


class ProjectSerializer(serializers.ModelSerializer):
    technologies = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()

    def get_image(self, obj):
        return obj.image.url

    @staticmethod
    def get_technologies(obj):
        return obj.technologies.get_queryset().values_list("icon", flat=True)

    class Meta:
        model = Project
        fields = "__all__"
