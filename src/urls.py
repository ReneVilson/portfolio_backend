from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from src import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('tinymce/', include('tinymce.urls')),
    path("api/", include("src.apps.api.urls")),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
