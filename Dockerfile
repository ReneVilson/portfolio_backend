FROM python:3.8
ENV PYTHONUNBUFFERED=1
WORKDIR /code
RUN apt update -y && apt upgrade -y && pip install --upgrade pip && apt-get install -y gcc
RUN apt-get install python-setuptools -y

COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
